package android.curso.serviciosweb1;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button get;
    private Button post;
    private TableLayout tabla;
    private TableRow fila;
    TableRow.LayoutParams layoutFila;

//    private String nuevo_nombre;
//    private String nuevo_depto;
//    private String nuevo_sueldo;

    private String nuevo_imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        get=(Button)findViewById(R.id.get);
        post=(Button)findViewById(R.id.post);
        tabla=(TableLayout)findViewById(R.id.tabla);

        layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);

//        get.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v)
//            {
//                tabla.removeAllViews();
//                new MetodoGet().execute();
//            }
//        });

        post.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Dialog popup = new Dialog(MainActivity.this);
                popup.setContentView(R.layout.popup);
                popup.setTitle("Envia IMEI");

//                final EditText Nnombre=(EditText)popup.findViewById(R.id.nuevo_nombre);
//                Spinner Ndepto=(Spinner)popup.findViewById(R.id.nuevo_departamento);
//                final EditText Nsueldo=(EditText)popup.findViewById(R.id.nuevo_sueldo);
//                Button enviar=(Button)popup.findViewById(R.id.enviar);

                final TextView Nimei = (TextView) popup.findViewById(R.id.deviceId);

                // fincion para obtener el imei de mi dispositivo
                TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                Nimei.setText(getDeviceID(tm));
                //--------------------------------------------------------------

                Button btnEnviar = (Button) popup.findViewById(R.id.enviar);

                popup.show();

                btnEnviar.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        popup.dismiss();
                        nuevo_imei = Nimei.getText().toString();
                        new MetodoPost().execute();
                    }
                });
            }
        });
    }

    private String getDeviceID(TelephonyManager pm){
        String id = pm.getDeviceId();
        if (id==null){
            id = "no esta disponible";
        }

        int pt = pm.getPhoneType();

        switch (pt){
            case TelephonyManager.PHONE_TYPE_NONE:
                return id;
            case TelephonyManager.PHONE_TYPE_GSM:
                return id;
            case TelephonyManager.PHONE_TYPE_CDMA:
                return id;
            default:
                return id;
        }
    }

   /* private void agregarFilas(String nombre,String departamento,String sueldo)
    {
        fila=new TableRow(this);
        fila.setLayoutParams(layoutFila);

        TextView nombre_empleado=new TextView(this);
        TextView departamento_empleado=new TextView(this);
        TextView sueldo_empleado=new TextView(this);

        nombre_empleado.setText(nombre);
        departamento_empleado.setText(departamento);
        sueldo_empleado.setText(sueldo);

        nombre_empleado.setBackgroundResource(R.drawable.celda_cuerpo);
        departamento_empleado.setBackgroundResource(R.drawable.celda_cuerpo);
        sueldo_empleado.setBackgroundResource(R.drawable.celda_cuerpo);


        sueldo_empleado.setGravity(Gravity.RIGHT);

        //Agregamos las vistas a la fila
        fila.addView(nombre_empleado);
        fila.addView(departamento_empleado);
        fila.addView(sueldo_empleado);
        tabla.addView(fila);

    }*/

    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> datos_empleados;

    private static String link_get = "http://10.1.6.38/monitoreo/cswUsuario/obtenerLogin";
    private static String link_post = "http://10.1.6.38/monitoreo/cswUsuario/obtenerLogin";

    private String resultado;
    JSONArray JsonArray = null;
    int success;
    JSONArray ArrayDatos = null;

/*    class MetodoGet extends AsyncTask<String, String, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Descargando datos de empleados");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args)
        {
            success=0;
            resultado="";
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try
            {
                JSONObject json = jParser.makeHttpRequest(link_get, "GET", params);
                Log.d("El resultado en JSON: ", json.toString());

                try {
                    success = json.getInt("correcto");
                    if (success == 1)
                    {
                        resultado="ok";

                        ArrayDatos = json.getJSONArray("empleados");
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }catch (NullPointerException e)
            {
                resultado="no";
            }


            return null;
        }
        protected void onPostExecute(String file_url)
        {
            pDialog.dismiss();

            if(resultado.compareTo("ok")==0)
            {
                agregarFilas("NOMBRE", "DEPARTAMENTO", "SUELDO");

                for (int i = 0; i < ArrayDatos.length(); i++)
                {
                    ContentValues values = new ContentValues();
                    JSONObject c;
                    try {
                        c = ArrayDatos.getJSONObject(i);
                        String nombre=c.getString("nombres");
                        String departamento=c.getString("departamento");
                        String sueldo=c.getString("sueldo");
                        agregarFilas(nombre, departamento, sueldo);
                    }
                    catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                Toast.makeText(getApplicationContext(),
                        "Lista de empleados actualizada", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(),
                        "Error en la conexión", Toast.LENGTH_SHORT).show();
            }

        }

    }*/

    class MetodoPost extends AsyncTask<String, String, String>
    {
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Enviando datos de mi IMEI");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args)
        {
            success=0;
            resultado="";
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("imei", nuevo_imei));

//            params.add(new BasicNameValuePair("nombres",nuevo_nombre));
//            params.add(new BasicNameValuePair("departamento",nuevo_depto));
//            params.add(new BasicNameValuePair("sueldo",nuevo_sueldo));

            try
            {
                JSONObject json = jParser.makeHttpRequest(link_post, "POST", params);

                Log.d("El resultado en JSON: ", json.toString());

                try {
                    success = json.getInt("correcto");
                    if (success == 1)
                    {
                        // hacer descarga de usuario y de manzanas
                        resultado="ok";
                    }
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    resultado="no";
                }

            }catch (NullPointerException e)
            {
                resultado="no";
            }
            return null;
        }

        protected void onPostExecute(String file_url)
        {
            pDialog.dismiss();

            if(resultado.compareTo("ok")==0)
            {
                Toast.makeText(getApplicationContext(),
                        "Cargar datos enviados de el servidor", Toast.LENGTH_SHORT).show();
                        // aqui recien se carga los datos de la base de datos del servidor

            }
            else
            {
                Toast.makeText(getApplicationContext(),
                        "Error en la conexión", Toast.LENGTH_SHORT).show();
            }

        }

    }
}
